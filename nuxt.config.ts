// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: false,
  css: [
    "primevue/resources/themes/lara-light-indigo/theme.css",
    "primeicons/primeicons.css",
    "primeflex/primeflex.css",
  ],
  runtimeConfig: {
    public: {
      apiBaseUrl:
        process.env.NUXT_PUBLIC_API_BASE_URL || "http://localhost:7001",
      mqttUri: process.env.NUXT_PUBLIC_MQTT_URI || "http://localhost:7001",
    },
  },
  modules: ["nuxt-primevue"],
  primevue: {
    components: {
      include: "*",
    },
    directives: {
      include: "*",
    },
    composables: {
      include: "*",
    },
  },
});
