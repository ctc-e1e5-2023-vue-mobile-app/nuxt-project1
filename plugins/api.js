import axios from "axios";
export default defineNuxtPlugin((nuxtApp) => {
  const token = localStorage.getItem("token");
  const baseURL = useRuntimeConfig().public.apiBaseUrl;
  const axiosInstance = axios.create({
    baseURL,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  nuxtApp.provide("api", axiosInstance);
});
