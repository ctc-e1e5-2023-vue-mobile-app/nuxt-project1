function parseJwt(token) {
  const base64 = token.split(".");
  if (!base64[1]) {
    return "";
  }
  return JSON.parse(atob(base64[1]));
}

export default defineNuxtPlugin((nuxtApp) => {
  const auth = {
    payload: {},
    isAuthenticated: false,
    refreshTokenInterval: null,
    setToken(token) {
      if (!token) {
        this.isAuthenticated = false;
        this.payload = null;
        return;
      }

      localStorage.setItem("token", token);
      this.payload = parseJwt(token);
      this.isAuthenticated = true;
      this.setApiHeaders();
    },
    getToken() {
      return localStorage.getItem("token");
    },
    setApiHeaders() {
      nuxtApp.$api.defaults.headers.common.Authorization = `Bearer ${this.getToken()}`;
      nuxtApp.$api.defaults.headers.Authorization = `Bearer ${this.getToken()}`;
    },
    logout() {
      this.setToken(null);
      this.stopRefreshTokenInterval();
    },
    async refreshToken() {
      this.setApiHeaders();
      const res = await nuxtApp.$api.get("/api/auth/refresh");
      token = res.data.data.token;
      if (!token) {
        this.startRefreshTokenTimer(1000 * 3);
      }

      this.setToken(res.data.data.token);
      this.startRefreshTokenTimer();
    },
    startRefreshTokenTimer(milliseconds = 1000 * 60) {
      this.setToken(this.getToken());
      this.stopRefreshTokenInterval();
      this.refreshTokenInterval = setInterval(() => {
        this.refreshToken();
      }, milliseconds);
    },
    stopRefreshTokenInterval() {
      if (this.refreshTokenInterval) {
        clearInterval(this.refreshTokenInterval);
      }
    },
  };

  nuxtApp.hook("app:mounted", () => {
    console.log("app:mounted");
    const token = auth.getToken();
    if (!token) {
      return;
    }
    auth.setToken(token);
    if (!auth.payload.exp) {
      return;
    }

    auth.startRefreshTokenTimer();
    // exp - now <= 2 minutes
    // if (auth.payload.exp - Date.now() / 1000 <= 60 * 2) {
    //   auth.refreshToken();
    // } else {
    //   auth.startRefreshTokenTimer();
    // }
  });

  nuxtApp.provide("auth", auth);
});
