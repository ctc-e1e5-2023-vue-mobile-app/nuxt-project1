export default defineNuxtPlugin((nuxtApp) => {
  const publicConfig = useRuntimeConfig().public;
  nuxtApp.provide("apiBaseUrl", publicConfig.apiBaseUrl);
});
