import { defineNuxtRouteMiddleware, navigateTo } from "#app";
export default defineNuxtRouteMiddleware((to, from) => {
  if (process.server) return;
  const auth = useNuxtApp().$auth;
  // auth.setToken();
  if (to.path === "/login" || to.path === "/") {
    return;
  }
  console.log("call middleware: auth");
  const token = auth.getToken();
  if (!token) {
    console.log("no token");
    return navigateTo("/login");
  }

  auth.setToken(token);

  if (!auth.isAuthenticated) {
    console.log("not auth");
    return navigateTo("/login");
  }
  if (auth.payload.exp <= new Date().getTime() / 1000) {
    console.log("token expired");
    return navigateTo("/login");
  }
});
